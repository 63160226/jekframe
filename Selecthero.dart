class Selecthero{
   var NAME = ['', ''];
  var MartialArt = ['', ''];
  var Rage = ['', ''];
  var HP = [0, 0];
  var ATK = [0, 0];
  var Attack = [0, 0];
  var Plus = [0, 0];
  var Check = [0, 0];
  SelectHero(int Select, int Player) {
    switch (Select) {
      case 1:
        NAME[Player] = 'Jin';
        MartialArt[Player] = 'Karate';
        HP[Player] = 150;
        ATK[Player] = 25;
        Plus[Player] = 75;
        break;
      case 2:
        NAME[Player] = 'Eddy';
        MartialArt[Player] = 'Karate';
        HP[Player] = 155;
        ATK[Player] = 20;
        Plus[Player] = 77;
        break;
      case 3:
        NAME[Player] = 'Steve';
        MartialArt[Player] = 'MuayThai';
        HP[Player] = 130;
        ATK[Player] = 30;
        Plus[Player] = 65;
        break;
      case 4:
        NAME[Player] = 'Negan';
        MartialArt[Player] = 'MuayThai';
        HP[Player] = 170;
        ATK[Player] = 15;
        Plus[Player] = 85;
        break;
      case 5:
        NAME[Player] = 'Bot';
        MartialArt[Player] = 'MuayThai';
        HP[Player] = 200;
        ATK[Player] = 10;
        Plus[Player] = 100;
        break;
      default:
    }
  }
}